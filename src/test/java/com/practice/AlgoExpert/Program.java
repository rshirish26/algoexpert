package com.practice.AlgoExpert;

import com.practice.AlgoExpert.ProgramTest.TestBST;

class Program {
//	static class BST {
//		public int value;
//		public BST left;
//		public BST right;
//
//		public BST(int value) {
//			this.value = value;
//		}
//
//		public BST insert(int value) {
//			BST currentNode = this;
//			while (true) {
//				if (value < currentNode.value) {
//					if (currentNode.left == null) {
//						BST newNode = new BST(value);
//						currentNode.left = newNode;
//						break;
//					} else {
//						currentNode = currentNode.left;
//					}
//				} else {
//					if (currentNode.right == null) {
//						BST newNode = new BST(value);
//						currentNode.right = newNode;
//						break;
//					} else {
//						currentNode = currentNode.right;
//					}
//				}
//				System.out.println("VALUE inside :  " + currentNode.value);
//			}
//			System.out.println("VALUE :  " + this.value);
//			return this;
//		}
//
//	public boolean contains(int value) {
//        BST currentNode = this;
//        while(currentNode!=null)
//        {
//           if(value < currentNode.value)
//           {
//            currentNode = currentNode.left;
//           }
//           else if (value > currentNode.value)  {
//            currentNode = currentNode.right;
//           }
//           else return true;
//         }
//        return false;
//      }
//
//		public BST remove(int value) {
//			// Write your code here.
//			// Do not edit the return statement of this method.
//			return this;
//		}
//
//		public static void main(String args[]) {
//			Program.BST test1 = new Program.BST(10);
//			test1.insert(5).insert(15).insert(5).insert(2).insert(14).insert	(22);
//			boolean boo = test1.contains(6);
//			System.out.println(boo);
//
//		}
//	}

	public static int findClosestValueInBst(BST tree, int target) {
	    BST currentNode = tree;
			int closest = Integer.MAX_VALUE;
			
			while(currentNode != null)
			{
				closest = Math.min(closest, Math.abs(currentNode.value - target));
				if(target < currentNode.value)
				{
					if (currentNode.left == null)
					{
						break;
					}
					currentNode = currentNode.left;
				}
				else if (target > currentNode.value)
				{
					if(currentNode.right == null)
					{
						break;
					}
					currentNode = currentNode.right;
				}
				else if (target == currentNode.value)
					return target;
				
			}
			return currentNode.value;
	  }

	  static class BST {
	    public int value;
	    public BST left;
	    public BST right;

	    public BST(int value) {
	      this.value = value;
	    }
	  }
}
