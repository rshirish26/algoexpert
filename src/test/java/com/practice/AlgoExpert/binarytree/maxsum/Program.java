package com.practice.AlgoExpert.binarytree.maxsum;

public class Program {

	public static Integer maxPathSum(BinaryTree tree) {
	    int root = tree.value;  
	    int left = 0;
	    int right = 0;
	    if(tree.left!= null)
	    {
	      left = findMaxSumfromRightSubTree(tree.left);
	      System.out.println("left sum :" +left);
	    }
	    if(tree.right != null)	
	    {
	     right =  findMaxSumfromRightSubTree(tree.right);
	     System.out.println("right sum :" +right);
	    }
	    
	    return root+ right + left;
	  }
			
		
		public static Integer findMaxSumfromRightSubTree(BinaryTree tree)
	   {  
	       int sum =0;
	      if(tree!= null)
	      {
	    	  System.out.println("Tree.value :" +tree.value);
	          sum = sum + tree.value;

	          findMaxSumfromRightSubTree(tree.right);
	      }
	      return sum;
	   }

			
	  

	  static class BinaryTree {
	    public int value;
	    public BinaryTree left;
	    public BinaryTree right;

	    public BinaryTree(int value) {
	      this.value = value;
	    }
	  }
	
}
