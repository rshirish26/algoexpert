package com.practice.AlgoExpert.binarytree.maxsum;

import static org.junit.Assert.assertTrue;

import java.util.ArrayDeque;

import org.junit.Test;

public class ProgramTest {
	

		  @Test
		  public void TestCase1() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(new int[] {2, 3}, 0);
		    assertTrue(Program.maxPathSum(test) == 6);
		  }

		  @Test
		  public void TestCase2() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(new int[] {2, -1}, 0);
		    assertTrue(Program.maxPathSum(test) == 3);
		  }

		  @Test
		  public void TestCase3() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(new int[] {-5, 3, 6}, 0);
		    assertTrue(Program.maxPathSum(test) == 6);
		  }

		  @Test
		  public void TestCase4() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(new int[] {2, 3, 4, 5, 6, 7}, 0);
		    assertTrue(Program.maxPathSum(test) == 18);
		  }

		  @Test
		  public void TestCase5() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(new int[] {-10, -5, 30, 45, -20, -21, 5, 1, 3, -3, 100, 2, 100, 1}, 0);
		    assertTrue(Program.maxPathSum(test) == 154);
		  }

		  @Test
		  public void TestCase6() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(new int[] {-10, -5, 30, 45, -20, -21, 5, 1, 3, -3, 100, 2, 100, 1, 100}, 0);
		    assertTrue(Program.maxPathSum(test) == 201);
		  }

		  @Test
		  public void TestCase7() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(new int[] {-10, -5, 30, 75, -20, -21, 5, 1, 3, -3, 100, 2, 100, 1, 100}, 0);
		    assertTrue(Program.maxPathSum(test) == 203);
		  }

		  @Test
		  public void TestCase8() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(
		        new int[] {
		          -150, -5, 30, 75, -20, -21, 5, 1, 3, -3, 100, 2, 100, 1, 100, 100, 5, 10, 150, -8
		        },
		        0);
		    assertTrue(Program.maxPathSum(test) == 228);
		  }

		  @Test
		  public void TestCase9() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(
		        new int[] {
		          -150, -5, 30, 75, -20, -21, 5, 1, 3, -3, 100, 2, 100, 1, 100, 100, 5, 10, 150, 151
		        },
		        0);
		    assertTrue(Program.maxPathSum(test) == 304);
		  }

		  @Test
		  public void TestCase10() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(
		        new int[] {
		          -5, -3, 0, 2, 2, 1, -3, 3, 1, 1, 0, 5, 1, 1, 0, 1, 1, -1, -1, -6, -1, -100, -9, -91, 2, 1,
		          0, 1, -5, 0
		        },
		        0);
		    assertTrue(Program.maxPathSum(test) == 9);
		  }

		  @Test
		  public void TestCase11() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(
		        new int[] {
		          -5, -3, 0, 2, 2, 1, -3, -4, 1, 1, 0, 5, 1, 1, 0, 1, 10, -1, -1, -6, -1, -100, -9, -91, 2,
		          1, 0, 1, -5, 0
		        },
		        0);
		    assertTrue(Program.maxPathSum(test) == 10);
		  }

		  @Test
		  public void TestCase12() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(
		        new int[] {
		          -5, -3, 0, 2, 2, 1, -3, -4, 1, 1, 0, 5, 1, 1, 0, 1, 3, -1, -1, -6, -1, -100, -9, -91, 2,
		          1, 0, 1, -5, 0, 3, 1, 2, 2, 7, -5
		        },
		        0);
		    assertTrue(Program.maxPathSum(test) == 10);
		  }

		  @Test
		  public void TestCase13() {
		    TestBinaryTree test = new TestBinaryTree(1);
		    test.insert(
		        new int[] {
		          -5, -3, 0, 2, 2, 1, -3, 3, 1, 1, 0, 5, 1, 1, 0, 1, 1, -1, -1, -6, -1, -100, -9, -91, 2, 1,
		          0, 1, 5, 0
		        },
		        0);
		    assertTrue(Program.maxPathSum(test) == 13);
		  }

		  class TestBinaryTree extends Program.BinaryTree {
		    public TestBinaryTree(int value) {
		      super(value);
		    }

		    public void insert(int[] values, int i) {
		      if (i >= values.length) {
		        return;
		      }
		      ArrayDeque<Program.BinaryTree> queue = new ArrayDeque<Program.BinaryTree>();
		      queue.addLast(this);
		      while (queue.size() > 0) {
		        Program.BinaryTree current = queue.pollFirst();
		        if (current.left == null) {
		          current.left = new Program.BinaryTree(values[i]);
		          break;
		        }
		        queue.addLast(current.left);
		        if (current.right == null) {
		          current.right = new Program.BinaryTree(values[i]);
		          break;
		        }
		        queue.addLast(current.right);
		      }
		      insert(values, i + 1);
		    }
		  }
		}

