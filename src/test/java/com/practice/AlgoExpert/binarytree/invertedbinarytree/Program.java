package com.practice.AlgoExpert.binarytree.invertedbinarytree;

public class Program {
	
	public static void invertBinaryTree(BinaryTree tree) {
	    if(tree.left != null)
	    {   
	    	
	    	    System.out.println("tree.left :" +tree.left.value);
	        invertBinaryTree(tree.left);
	        if(tree.left.left != null)
	        {
	        System.out.println("inside left swap : "+tree.left.value);
	        swap(tree.left);
	        }
	    }
	    if(tree.right != null)
	    {
	    	     System.out.println("tree.right :" +tree.right.value);
	        invertBinaryTree(tree.right);
	        if(tree.right.left != null || tree.right.right !=null)
	        {
	        	System.out.println("inside right swap : "+tree.right.value);
	        swap(tree.right);
	        }
	    }
	    

	  }
		
		public static BinaryTree swap(BinaryTree tree){
	      BinaryTree temp = tree.left;
	      tree.left = tree.right;
	      tree.right = temp;

	      return tree;
	  }

	  static class BinaryTree {
	    public int value;
	    public BinaryTree left;
	    public BinaryTree right;

	    public BinaryTree(int value) {
	      this.value = value;
	    }
	  }

}
