package com.practice.AlgoExpert.Stack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinMaxStack {

	public static void main(String[] args) {
		 Program12.MinMaxStack stack = new Program12.MinMaxStack();
		    stack.push(2);
		   
		    Integer min = stack.getMin();
			 Integer  max = stack.getMax();
			   Integer peek = stack.peek();
		    stack.push(7);
		   Integer min1 = stack.getMin();
		   Integer  max1 = stack.getMax();
		   Integer peek1= stack.peek();

	}
	 
	

}

class Program12 {
	
	static class MinMaxStack {
		
		List<Map<String, Integer>> minMaxStack = new ArrayList<Map<String, Integer>>();
		List<Integer> stack = new ArrayList<Integer>() ;
		
	    public Integer peek() {
	      return stack.get(stack.size() -1);
	    }

	    public Integer pop() {
	    		minMaxStack.remove(minMaxStack.size() -1);	
	      return  stack.remove(stack.size() -1);
	    }

	    public void push(Integer number) {
	      Map<String, Integer> newMinMax = new HashMap<String, Integer>();
	      newMinMax.put("min", number);
	      newMinMax.put("max", number);
	      
	      if(minMaxStack.size() > 0)
	      {
	    	  Map<String, Integer> lastMinMax = new HashMap<String, Integer>(minMaxStack.get(minMaxStack.size()-1));
	    	  newMinMax.replace("min", Math.min(lastMinMax.get("min"), number));
	    	  newMinMax.replace("max", Math.min(lastMinMax.get("max"), number));
	      }
	      minMaxStack.add(newMinMax);
	      stack.add(number);
	    }
	    

	    public Integer getMin() {
	     return minMaxStack.get(minMaxStack.size()-1).get("min");
	    }

	    public Integer getMax() {
	    	return minMaxStack.get(minMaxStack.size()-1).get("max");
	    }
	  }
	
}