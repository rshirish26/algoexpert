package com.practice.AlgoExpert.Stack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BalanceBracket {

	public static void main(String[] args) {
		 String input = "([])(){}(())()()";
		 boolean result = Program11.balancedBrackets(input);
		 System.out.println("Result :" +result);

	}

}

class Program11 {

	public static boolean balancedBrackets(String input) {
		String openingBracket = "([{";
		String closingBracket = ")]}";
		Map<Character, Character> mapOfBrackets = new HashMap<Character, Character>();
		mapOfBrackets.put(')', '(');
		mapOfBrackets.put('}', '{');
		mapOfBrackets.put(']', '[');
		List<Character> stack = new ArrayList<Character>();
		
		for(int i = 0; i< input.length() ; i++)
		{
			char value = input.charAt(i);
			
			if(openingBracket.indexOf(value) != -1)
			{
				stack.add(value);
			}
			else if (closingBracket.indexOf(value) != -1)
			{
				if(stack.size() == 0)
					return false;
			    if(stack.get(stack.size() -1) == mapOfBrackets.get(value)) {
			    	stack.remove(stack.size()-1);
			    }
			    else {
			    	return false;
			    }

			}
		}
		
		return stack.size() == 0;
		
	}
	
}