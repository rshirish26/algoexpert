package com.practice.AlgoExpert.Tries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultiStringSearch {

	public static void main(String[] args) {
		boolean[] expected = {true, false, true, true, false, true, false};
	    List<Boolean> output =
	        Program1.multiStringSearch(
	            "Mary goes to the shopping center every week",
	            new String[] {"shop", "shopping", "string", "kappa"});
	    System.out.println("Output :" +output);

	}

}

class Program1 {
	
	public static List<Boolean> multiStringSearch(String string, String[] strings) {
		Trie trie = new Trie();
		for (String str : strings)
		{
		 trie.insert(str); 
		}
		Map<String, Boolean> containedString = new HashMap<String, Boolean>(); 
		for(int i =0; i < string.length(); i++)
		{
		 findSmallestStringIn(i, trie, containedString, string);
		}
		List<Boolean> result = new ArrayList<Boolean>();
		for(String check : strings)
		{
			result.add(containedString.containsKey(check));
		}
		
		return result;
	}
	
	private static void findSmallestStringIn(int i, Trie trie, Map<String, Boolean> containedString, String string) {
		TrieNode  currentNode = trie.root;
		for(int j = i; j< string.length(); j++)
		{
			char letter = string.charAt(j);
			if(!currentNode.children.containsKey(letter))
			{
				break;
			}
			currentNode = currentNode.children.get(letter);
			if(currentNode.children.containsKey(trie.endSymbol))
				containedString.put(currentNode.word, true);
		}
		
		
	}

	static class TrieNode {
		Map<Character, TrieNode>  children = new HashMap<Character, TrieNode>();
		String word;
	}
	
	static class Trie {
		TrieNode root = new TrieNode();
		char endSymbol = '*';
		
		public void insert(String str)
		{
			TrieNode node = root;
			for(int i= 0; i< str.length() ;i++)
			{
				char letter = str.charAt(i);
				if(!node.children.containsKey(letter))
				{
					TrieNode newNode = new TrieNode();
					node.children.put(letter, newNode);
				}
				node = node.children.get(letter);
			}
			node.children.put(endSymbol, null);
			node.word = str;
		}
		
		
	}
	
	
	
	
}
