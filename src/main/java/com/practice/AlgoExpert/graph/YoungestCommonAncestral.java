package com.practice.AlgoExpert.graph;

import java.util.HashMap;
import java.util.Map;

import com.practice.AlgoExpert.graph.Program10Test.Program10;


public class YoungestCommonAncestral {

	public static void main(String[] args) {
		  Program10.AncestralTree yca =
				Program10.getYoungestCommonAncestor(
			        		Program10Test.ancestralTrees.get('A'), Program10Test.ancestralTrees.get('G'), Program10Test.ancestralTrees.get('M'));
	}

}

class Program10Test {
	static Map<Character, Program10.AncestralTree> ancestralTrees;
	static String alphabet;

	static {
		ancestralTrees = new HashMap<Character, Program10.AncestralTree>();
		alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for (char a : alphabet.toCharArray()) {
			ancestralTrees.put(a, new Program10.AncestralTree(a));
		}

		ancestralTrees.get('A').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('B'),
				ancestralTrees.get('C'), ancestralTrees.get('D'), ancestralTrees.get('E'), ancestralTrees.get('F') });
		ancestralTrees.get('B').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('G'),
				ancestralTrees.get('H'), ancestralTrees.get('I') });
		ancestralTrees.get('C').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('J') });
		ancestralTrees.get('D')
				.addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('K'), ancestralTrees.get('L') });
		ancestralTrees.get('F')
				.addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('M'), ancestralTrees.get('N') });
		ancestralTrees.get('H').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('O'),
				ancestralTrees.get('P'), ancestralTrees.get('Q'), ancestralTrees.get('R') });
		ancestralTrees.get('K').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('S') });
		ancestralTrees.get('P')
				.addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('T'), ancestralTrees.get('U') });
		ancestralTrees.get('R').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('V') });
		ancestralTrees.get('V').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('W'),
				ancestralTrees.get('X'), ancestralTrees.get('Y') });
		ancestralTrees.get('X').addAsAncestor(new Program10.AncestralTree[] { ancestralTrees.get('Z') });
	}

	static class Program10 {

		static class AncestralTree {
			public char name;
			public AncestralTree ancestor;

			AncestralTree(char name) {
				this.name = name;
				this.ancestor = null;
			}

			// This method is for testing only.
			void addAsAncestor(AncestralTree[] descendants) {
				for (AncestralTree descendant : descendants) {
					descendant.ancestor = this;
				}
			}
		}

		public static AncestralTree getYoungestCommonAncestor(AncestralTree topAncestor, AncestralTree decendentOne,
				AncestralTree decendentTwo) {
			
			int depthOne = getDecendentDepth(topAncestor, decendentOne);
			int depthTwo = getDecendentDepth(topAncestor, decendentTwo);
			
			if(depthOne > depthTwo)
			{
				return backTrackAncestralTree(decendentOne, decendentTwo, depthOne-depthTwo);
			}else
			{
				return backTrackAncestralTree(decendentTwo, decendentOne, depthTwo-depthOne);
			}

		}

		private static AncestralTree backTrackAncestralTree(AncestralTree lowerDecendent, AncestralTree higherDecendent, int diff) {
			
			while(diff > 0)
			{
				lowerDecendent = lowerDecendent.ancestor;
				diff--;
			}
			while (lowerDecendent != higherDecendent)
			{
				lowerDecendent = lowerDecendent.ancestor;
				higherDecendent = higherDecendent.ancestor;
			}
			return lowerDecendent;
		}

		private static int getDecendentDepth(AncestralTree topAncestor, AncestralTree decendent) {
			int depth = 0;
			while(decendent!= topAncestor)
			{
				depth++;
				decendent = decendent.ancestor;
			}
			
			return depth;
		}
	}

}