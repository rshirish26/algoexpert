package com.practice.AlgoExpert.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class RiverSizeProblem {

	public static void main(String[] args) {
		int[][] input = {
			      {1, 0, 0, 1, 0},
			      {1, 0, 1, 0, 0},
			      {0, 0, 1, 0, 1},
			      {1, 0, 1, 0, 1},
			      {1, 0, 1, 1, 0},
			    };
			    int[] expected = {1, 2, 2, 2, 5};
			    List<Integer> output = Program9.riverSizes(input);
			    Collections.sort(output);
			    System.out.println("Result "  +output.toString());
	}

}

class Program9 
{
	public static List<Integer> riverSizes(int[][] matrix) {
		
		List<Integer> sizeOfRivers = new ArrayList<Integer>();
		boolean[][] visited = new boolean[matrix.length][matrix[0].length];
		
		
		for(int i=0; i<matrix.length; i++)
		{
			for(int j=0; j<matrix[0].length; j++)
			{
				if(visited[i][j])
					continue;
				traverseNode(i, j, matrix, visited, sizeOfRivers);
			}
		}
	
		return sizeOfRivers;
	}

	private static void traverseNode(int i, int j, int[][] matrix, boolean[][] visited, List<Integer> sizeofRiver) {
	  
		int currentRiverSize = 0;
		List<Integer[]> nodeToExplore = new ArrayList<Integer[]>();
		nodeToExplore.add(new Integer[] {i, j});
		
		while(!nodeToExplore.isEmpty())
		{
			Integer[] currentNode = nodeToExplore.get(nodeToExplore.size() -1);
			nodeToExplore.remove(nodeToExplore.size() -1);
			int x= currentNode[0];
			int y= currentNode[1];
			
	       if(visited[x][y])
	    	   continue;
	       
	       visited[x][y] = true;
	       
	       if(matrix[x][y] == 0)
	    	   continue;
	       currentRiverSize++;
	       
	       List<Integer[]> unVisitedNeighbours = getUnvisitedNeighbours(matrix, visited, x, y);
	       
	       for(Integer[] neighbour : unVisitedNeighbours)
	       {
	    	   nodeToExplore.add(neighbour);
	       }    
		}
		 if(currentRiverSize > 0)
		 {
			 sizeofRiver.add(currentRiverSize);
		 }
		
	}

	private static List<Integer[]> getUnvisitedNeighbours(int[][] matrix, boolean[][] visited, int x, int y) {
		
		List<Integer[]> unVisitedNeighbours = new ArrayList<Integer[]>();
		 if(x > 0 && !visited[x-1][y]) {
			 unVisitedNeighbours.add(new Integer[] {x-1, y});
		 }
		 if(x < matrix.length -1 && !visited[x+1][y]) {
			 unVisitedNeighbours.add(new Integer[] {x+1, y});
		 }
		 if(y > 0 && !visited[x][y-1]) {
			 unVisitedNeighbours.add(new Integer[] {x, y-1});
		 }
		 if(y < matrix[0].length -1 && !visited[x][y+1]) {
			 unVisitedNeighbours.add(new Integer[] {x, y+1});
		 }
		 
		return unVisitedNeighbours;
	}
	
}
