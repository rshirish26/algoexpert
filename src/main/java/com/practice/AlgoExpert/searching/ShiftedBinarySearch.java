package com.practice.AlgoExpert.searching;

public class ShiftedBinarySearch {

	public static void main(String[] args) {
		int index = Program6.shiftedBinarySearch(new int[] { 45, 61, 71, 72, 73, 0, 1, 21, 33, 45 }, 33);
		System.out.println("Result :  " + index);
	}

}

class Program6 {

	public static int shiftedBinarySearch(int[] array, int target) {

		return shiftedBinarySearchOperation(array, target, 0, array.length - 1);

	}

	private static int shiftedBinarySearchOperation(int[] array, int target, int left, int right) {
		 
		while(left <= right)
		{
			int middle = (left+right)/2;
			int potentialMatch = array[middle];
			int leftValue = array[left];
			int rightValue = array[right];
			
			if(target == potentialMatch)
                return middle;			
			else if(leftValue <= potentialMatch)
			{
				if(target < potentialMatch && target >= leftValue)
				{
					right = middle -1;
				}
				else 
					left  = middle +1;
			}
			else 
			{
				if (target > potentialMatch && target <= rightValue)
				{
					left = middle +1;
				}
				else 
					right = middle -1;
			}
		}
		
		
		return -1;
	}
}