package com.practice.AlgoExpert.searching;

public class QuickSelect {

	public static void main(String[] args) {
		
      int result = Program8.quickselect(new int[] {8, 5, 2, 9, 7, 6, 3}, 3);
      System.out.println("Resukt : "+result);
	}

}

class Program8 
{

	public static int quickselect(int[] array, int i) {
		int position = i -1;
		return quickSelect(array, 0, array.length-1, position);
	}

	private static int quickSelect(int[] array, int startIdx, int endIdx, int position) {
		while(true)
		{
			if (startIdx > endIdx)
			{
				throw new RuntimeException("Go to hell");
			}
			int leftPointer = startIdx +1;
			int rightPointer = endIdx;
			int pivote = startIdx;
			while (leftPointer <= rightPointer)
			{
			if(array[leftPointer] >= array[pivote] && array[rightPointer] < array[pivote])
				swap(leftPointer, rightPointer, array);
			if(array[leftPointer] <=	 array[pivote])
				leftPointer++;
			if(array[rightPointer] >= array[pivote])
				rightPointer--;
			}
			swap( pivote, rightPointer, array);
			
			if (rightPointer == position)
				return array[rightPointer];
			else if(rightPointer > position)
			{
				endIdx = rightPointer -1;
			} else 
			{
				startIdx = rightPointer + 1;
			}
		}
	

	}

	private static void swap(int i, int j, int[] array) {
	  int temp = array[j];
	  array[j] = array[i];
	  array[i] = temp;
	}
	
}
