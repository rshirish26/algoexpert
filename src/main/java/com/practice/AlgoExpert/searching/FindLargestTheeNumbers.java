package com.practice.AlgoExpert.searching;

import java.util.Arrays;

public class FindLargestTheeNumbers {

	public static void main(String[] args) {
	
		 int[] expected = {11, 43, 55};
		 int[] result = Program3.findThreeLargestNumbers(new int[] {7, 8, 3, 11, 43, 55});
		 System.out.println("Result :" + Arrays.toString(result));
	}

}
class Program3 {

	public static int[] findThreeLargestNumbers(int[] array) {
		
		int [] threeLargest = new int[] {Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};
		for (int value : array)
		{
			updateLargest(value, threeLargest);
		}
		return threeLargest;
	}

	private static void updateLargest(int value, int[] threeLargest) {
		if(value >threeLargest[2] ) {
			shiftValue(value, 2, threeLargest);
		} else if (value > threeLargest[1])
		{
			shiftValue(value, 1, threeLargest);
		}else if (value > threeLargest[0])
		{
			shiftValue(value, 0, threeLargest);
		}
		
	}

	private static void shiftValue(int value, int idx, int[] threeLargest) {
		for (int i =0 ; i<= idx; i++ ) {
			
			if(idx == i)
			{
				threeLargest[i] =value;
			}
			else 
				threeLargest[i] = threeLargest[i+1];
		}
		
	}
	
}
