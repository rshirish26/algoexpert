package com.practice.AlgoExpert.searching;

import java.util.Arrays;

public class SearchInSortedMatrix {
	
	public static void main(String args[])
	{
		 int[][] matrix = {
				    {1, 4, 7, 12, 15, 1000},
				    {2, 5, 19, 31, 32, 1001},
				    {3, 8, 24, 33, 35, 1002},
				    {40, 41, 42, 44, 45, 1003},
				    {99, 100, 103, 106, 128, 1004},
				  };
		 
		  int[] expected = {4, 4};
		  int[] output = Program4.searchInSortedMatrix(matrix, 128);
		  System.out.println("Result : "+Arrays.toString(output));
		 
	}

}

class Program4 {

	public static int[] searchInSortedMatrix(int[][] matrix, int value) {
		
		 int row = 0;
		 int column = matrix[0].length -1;
		  while (row < matrix.length && column >=0)
		  {
			  if(matrix[row][column] > value )
				  column--;
			  else if (matrix[row][column] < value )
				  row++;
		      else 
		    	  return new int[] {row, column};
		  }
		  return new int[] {-1, -1};

	}
	
}