package com.practice.AlgoExpert.searching;

import java.util.Arrays;

public class SearchForRange {

	public static void main(String[] args) {

		int[] expected = { 4, 9 };
		int[] output = Program7.searchForRange(new int[] { 0, 1, 21, 33, 45, 45, 45, 45, 45, 45, 61, 71, 73 }, 45);
		System.out.println("Result : " + Arrays.toString(output));

	}

}

class Program7 {

	public static int[] searchForRange(int[] array, int target) {

		int[] finalResult = new int[] { -1, -1 };
		alteredBinarySearch(finalResult, array, 0, array.length - 1, target, true);
		alteredBinarySearch(finalResult, array, 0, array.length - 1, target, false);
		return finalResult;

	}

	private static void alteredBinarySearch(int[] finalResult, int[] array, int left, int right, int target, boolean goLeft) {
		
		while (left <= right)
		{
			int middle = (left+right)/2;
			if(target < array[middle])
			{
			   right = middle -1;
				
			}else if (target > array[middle])
			{
				left = middle +1;	
			}else 
			{
				if(goLeft)
				{
					if(middle == 0 || array[middle -1] != target)
					{
						finalResult[0] = middle;
						return;
					}
					else 
						right = middle -1;
				}
				else
				{
					if(middle == array.length -1 || array[middle + 1] != target )
					{
					   finalResult[1] = middle;
					   return;
					}
					else 
					{
                       left = middle+1 ;
					}
				}
			}	
		}
	}
}
