package com.practice.AlgoExpert.arrays;

import java.util.Arrays;
import java.util.stream.*;

public class MinReward {
	public static void main(String[] args) {
		int result = MinReward.minReward(new int[] {8, 4, 2, 1, 3, 6, 7, 9, 5});
		
	}
	
	public static int minReward(int[] array)
	{
		int [] rewards = new int [array.length];
		Arrays.fill(rewards, 1);
		
		for(int i=0 ; i < array.length -1; i++)
		{
			if(array[i] < array[i+1])
			{
				rewards[i+1] = rewards[i] +1;
			}
		}
		for(int i = array.length -1; i> 0; i--)
		{
			if(array[i] < array[i-1])
			{
				rewards[i-1] = rewards[i] +1;
			}
		}
		
		System.out.println("Result :" +Arrays.toString(rewards));
		
		return Arrays.stream(rewards).sum();
		
	}

}
