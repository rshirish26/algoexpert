package com.practice.AlgoExpert.arrays;

import java.util.Arrays;

public class LargestRange {
	public static void main(String[] args) {
		
		//int[] value =LargestRange.largestRange(new int[] {1, 2});
		int[] value =LargestRange.largestRange(new int[] {
                0, 9, 19, -1, 18, 17, 2, 10, 3, 12, 5, 16, 4, 11, 8, 7, 6, 15, 12, 12, 2, 1, 6,
                13, 14
              });
		System.out.print("Result : " + Arrays.toString(value));
	}

	private static int[] largestRange(int[] array) {
		Arrays.sort(array);
		int count =0;
		int range = Integer.MIN_VALUE;
		int smallest = Integer.MIN_VALUE;
		int largest = Integer.MAX_VALUE;
		System.out.println("sorted Array" +Arrays.toString(array));
		
		for(int i=0; i< array.length-1; i++)
		{ 
			if(array[i] != array[i+1]||(array[i] + 1) != array[i+1] )
			{
				System.out.println("Count" +count);
				if(count > range)
				{
					
				smallest = array[i] - count;
				largest = array[i];
				range = count; 
				count =-1;
				}
			}
			count ++;
		}
		if(array.length == 1)
			return new int[] {1,1};
		return new int[] {smallest, largest};
	
	
	}
		
		
	}


