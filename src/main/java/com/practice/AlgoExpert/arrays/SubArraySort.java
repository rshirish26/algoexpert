package com.practice.AlgoExpert.arrays;

import java.util.Arrays;

public class SubArraySort {
	public static void main(String[] args) {
		
		int [] result = SubArraySort.subarraySort(new int[] {4, 8, 7, 12, 11, 9, -1, 3, 9, 16, -15, 51, 7});
		System.out.println("Result" +Arrays.toString(result));
	}

	private static int[] subarraySort(int[] array) {
		int minOOO = Integer.MAX_VALUE;
		int maxOOO = Integer.MIN_VALUE;
		
		for(int i=0 ; i< array.length; i++)
		{
			int num = array[i];
			if(outOfOrder(i, num, array))
			{ 
			   System.out.println("Here");	
				minOOO = Math.min(minOOO, num);
				maxOOO = Math.max(maxOOO, num);
			}
		}

		if(minOOO == Integer.MAX_VALUE )
			return new int[] {-1, -1};
		int startCount =0;
		while (minOOO >= array[startCount])
		{
			startCount++;
		}
		int endCount = array.length -1;
		while (maxOOO <= array[endCount	])
		{
			endCount--;
		}
		return new int[] {startCount, endCount};
		
		
		

	}		
	
	private static boolean outOfOrder(int i, int num, int[] array)
	{
		 if(i == 0)
		{
			return array[i+1] < num;
		}
		if(i == array.length-1)
		{
			return num < array[i-1];
		}
		 return num > array[i+1] || num < array[i-1];
	}
		
	}
	
	
