package com.practice.AlgoExpert.arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class ZigZagTraverse {

	public static void main(String[] args) {
		  ArrayList<ArrayList<Integer>> test = new ArrayList<ArrayList<Integer>>();
		    test.add(new ArrayList<Integer>(Arrays.asList(1, 3, 4, 10)));
		    test.add(new ArrayList<Integer>(Arrays.asList(2, 5, 9, 11)));
		    test.add(new ArrayList<Integer>(Arrays.asList(6, 8,12, 15)));
		    test.add(new ArrayList<Integer>(Arrays.asList(7, 13, 14 , 16)));
		    ArrayList<Integer> result = ZigZagTraverse.zigzagTraverse(test);
		    System.out.println("Final : " +Arrays.toString(result.toArray()));
		
	}
	
	
	public static ArrayList<Integer> zigzagTraverse(ArrayList<ArrayList<Integer>> array) {
	    // Write 
			int height = array.size() -1;
			int width = array.get(0).size() -1;
			boolean down = true;
			int row =0;
			int column =0;
			ArrayList<Integer> result = new ArrayList<Integer>();
	
			
			while(!isLimitingCondition(row, column, height, width))
			{
				result.add(array.get(row).get(column));
				
				if(down == true && row == 0)
				{
					column = column +1;
					//System.out.println("1st = "+array.get(row).get(column));
					//result.add(array.get(row).get(column));
					System.out.println("1st = "+array.get(column).get(row));
					result.add(array.get(column).get(row));
					down = false;
					
				}
//				else if(down == true && row == width )
//				{
//					down = false;
//					row = row +1;
//					down = true;
//					System.out.println("6st = "+array.get(column).get(row));
//					result.add(array.get(column).get(row));
//				}
				else if(down == false && column == 0)
				{
					row = row +1;
					down = true;
					System.out.println("2st = "+array.get(column).get(row));
					result.add(array.get(column).get(row));
				}
//				else if(down == false && column == height)
//				{
//					down = true;
//					column = column +1;
//					//System.out.println("1st = "+array.get(row).get(column));
//					//result.add(array.get(row).get(column));
//					System.out.println("5st = "+array.get(column).get(row));
//					result.add(array.get(column).get(row));
//				}
				else if(down == true)
				{
					row = row -1;
					column = column +1;
//					System.out.println("3rd = "+array.get(row).get(column));
//					result.add(array.get(row).get(column));
					System.out.println("3st = "+array.get(column).get(row));
					result.add(array.get(column).get(row));
					
				}
				else if (down == false)
				{
					row = row +1;
					column = column - 1;
//					System.out.println("4th = "+array.get(row).get(column));
//					result.add(array.get(row).get(column));
					System.out.println("4st = "+array.get(column).get(row));
					result.add(array.get(column).get(row));
					
				}
				
				
			}
			return result;
		}
			
			
			public static boolean isLimitingCondition(int row, int column, int height, int width)
			{
				return (row > width || row < 0 || column > height || column <0);
			}
			
			
			
	  
}

