package com.practice.AlgoExpert.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ThreeNumberSum {

	public static void main(String args[]) {

		
	   ArrayList<Integer[]> output = ThreeNumberSum.threeNumberSum2(new int[] {12, 3, 1, 2, -6, 5, -8, 6},0);
	    System.out.println("Final :" + Arrays.toString(output.get(0)));
	    System.out.println("Final :" + Arrays.toString(output.get(1)));
	    System.out.println("Final :" + Arrays.toString(output.get(2)));
	    
//	    ArrayList<Integer[]> expected = new ArrayList<Integer[]>();
//	    expected.add(new Integer[] {-8, 2, 6});
//	    expected.add(new Integer[] {-8, 3, 5});
//	    expected.add(new Integer[] {-6, 1, 5});
	    
	    
	}
    public static ArrayList<Integer[]> threeNumberSum(int[] array, int targetValue)
			{
				ArrayList<Integer[]> expected = new ArrayList<Integer[]>();
				Arrays.sort(array);
				
				for(int i= 0; i<array.length; i++)
				{
					int value1=array[i];
					 System.out.println("Value 1: "+value1);
					for(int j =i+1; j< array.length; j++)
					{
						int value2 = array[j];
						System.out.println("Value 2: "+value2);
						for(int k = j+1; k<array.length; k++)
						{
							int value3 = array[k];
							System.out.println("Value 3: "+value3);
							if(targetValue == value1+ value2+ value3)
							{

								Integer[] sortedValue = new Integer[] {value1, value2, value3};
								Arrays.sort(sortedValue);
								expected.add(sortedValue);

								
							}
						}
					}
				}
				
				
			
				return expected;
				
				
				
			}

    public static ArrayList<Integer[]> threeNumberSum2(int[] array, int targetSum)
    {
        Arrays.sort(array);
    		ArrayList<Integer[]> expected = new ArrayList<Integer[]>();	
    		for(int i=0; i< array.length -2 ;i++)
    		{
    			int left = i+1;
    			int right = array.length-1;
    			
    			while (left < right)
    			{
    				int currentSum = array[i] + array[left] + array[right];
    				if(currentSum == targetSum)
    				{ 
    					expected.add(new Integer[] {array[i] , array[left], array[right]});
    					right--;
    					left++;
    				}
    				else if (currentSum < targetSum)
    				{
    					left++;
    				}
    				else if (targetSum < currentSum)
    				{
    					right--;
    				}
    			}		
    		}
    		return expected;
	
	}
			
		     
		

	
}
