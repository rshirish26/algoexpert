package com.practice.AlgoExpert.arrays;

import java.util.Arrays;

public class SmallestDifference {
	
	public static void main(String args[])
	{
	 int[] result = 	SmallestDifference.smallestDifference(new int[] {0, 20}, new int[] {21, -2});
	 System.out.println("Final Result : " +Arrays.toString(result));
	}
    
	public static int[] smallestDifference(int[] arrayOne, int[] arrayTwo)
	{
		Arrays.sort(arrayOne);
		Arrays.sort(arrayTwo);
		
		System.out.println("1st :" + Arrays.toString(arrayOne));
		System.out.println("2nd :" + Arrays.toString(arrayTwo));
		int smallest = Integer.MAX_VALUE;
		int current = Integer.MAX_VALUE;
		int ione = 0;
		int itwo = 0;
		int[] result = new int[0]; 
		
		while(ione < arrayOne.length && itwo < arrayTwo.length)
		{ 
			int valueOne = arrayOne[ione];
			int valueTwo = arrayTwo[itwo];
			System.out.println("1st :" +valueOne);
			System.out.println("2st :" +valueTwo);
			if(valueOne < valueTwo)
			{
				current = valueTwo - valueOne;
				ione++;
			}
			else if (valueOne > valueTwo){
				current = valueOne - valueTwo;
				itwo++;
			}
			else {
				return new int[] {valueOne, valueTwo};
			}
			if(current < smallest)
			{ 
				smallest = current;
				result = new int[] {valueOne , valueTwo};
			}
		}
		
		
		
		
		return result;
  }
	
}

