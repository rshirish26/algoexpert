package com.practice.AlgoExpert.LinkedList;

public class FindLoop {

	public static void main(String[] args) {
		 TestLinkedList test5 = new TestLinkedList(0);
		    test5.addMany(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9});
		    test5.getNthNode(10).next = test5.getNthNode(5);
	          Program13.findLoop(test5);

	}

}

class TestLinkedList extends Program13.LinkedList {
    public TestLinkedList(int value) {
      super(value);
    }

    public void addMany(int[] values) {
      Program13.LinkedList current = this;
      while (current.next != null) {
        current = current.next;
      }
      for (int value : values) {
        current.next = new Program13.LinkedList(value);
        current = current.next;
      }
    }

    public Program13.LinkedList getNthNode(int n) {
      int counter = 1;
      Program13.LinkedList current = this;
      while (counter < n) {
        current = current.next;
        counter++;
      }
      return current;
    }
  }

class Program13 {
	public static LinkedList findLoop(LinkedList head) {
		LinkedList first = head.next;
		LinkedList second = head.next.next;
		
		while (first != second)
		{
			first = first.next;
			second = second.next.next;
		}
		first = head;
		while(first != second)
		{
			first = first.next;
			second = second.next;
		}
		return first;
	}

	static class LinkedList {
		int value;
		LinkedList next = null;

		public LinkedList(int value) {
			this.value = value;
		}
	}
}
