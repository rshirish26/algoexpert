package com.practice.AlgoExpert.LinkedList;

import java.util.ArrayList;
import java.util.List;

public class ReverseLinkedList {

	public static void main(String[] args) {
		Program12.LinkedList test = newLinkedList(new int[] {0, 1, 2, 3, 4, 5});
	    List<Integer> result = toArrayList(Program12.reverseLinkedList(test));
	    int[] expected = new int[] {5, 4, 3, 2, 1, 0};
	    System.out.println("Result : " +result);

	}
	
	 public static List<Integer> toArrayList(Program12.LinkedList ll) {
		    List<Integer> arr = new ArrayList<Integer>();
		    Program12.LinkedList current = ll;
		    while (current != null) {
		      arr.add(current.value);
		      current = current.next;
		    }
		    return arr;
		  }
	 
	 public static Program12.LinkedList newLinkedList(int[] values) {
		    Program12.LinkedList ll = new Program12.LinkedList(values[0]);
		    Program12.LinkedList current = ll;
		    for (int i = 1; i < values.length; i++) {
		      current.next = new Program12.LinkedList(values[i]);
		      current = current.next;
		    }
		    return ll;
		  }

}

class Program12 {
	
	public static LinkedList reverseLinkedList(LinkedList head) {
		LinkedList p1 = null;
		LinkedList p2 = head;
		
		while(p2 != null)
		{
			LinkedList p3 = p2.next;
			p2.next = p1;
			p1 = p2;
			p2= p3;
		}
		return p1;
	  }

	  static class LinkedList {
	    int value;
	    LinkedList next = null;

	    public LinkedList(int value) {
	      this.value = value;
	    }
	  }
	
}
